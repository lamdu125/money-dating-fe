module.exports = {
  purge: [
    './src/**/*.{js,jsx,ts,tsx}',
    './public/index.html',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      'phone': '300px',
      'sm': '640px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px'
    },
    extend: {
      colors: {
        accessPrimaryButton: 'rgb(0, 149, 246)',
        accessPrimaryButtonHover: 'rgb(24, 119, 242)',
        accessSecondaryText: 'rgb(115, 115, 115)',
        accessPrimaryText: 'rgb(0, 0, 0)',
        accessTextWhite: 'rgb(255, 255, 255)',
        accessSecondaryBackground: 'rgb(250, 250, 250)',
        accessPrimaryBackground: 'rgb(255, 255, 255)',
        borderStroke: 'rgb(219, 219, 219)',
        accessSecondaryButton: 'rgb(38, 38, 38)',
        accessLink: 'rgb(0, 55, 107)',
        borderTable: 'hsl(240 3.7% 15.9%)',
        mdPrimaryText: 'rgb(245, 245, 245)',
        danger: {
          light: '#ef5a6a',
          DEFAULT: '#c90339',
          dark: '#b20232',
        },
      },
    },
  },
  variants: {
    extend: {
      display: ["group-hover"]
    },
  },
  plugins: [
  ],
  content: [
    "./node_modules/tailwind-datepicker-react/dist/**/*.js",
    "./src/**/*.{js,jsx,ts,tsx}",
  ]
};
