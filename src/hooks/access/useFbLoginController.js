import { useEffect, useState } from "react";
import { initFacebookSdk } from "services/facebook_services/facebookLogin";
import toast from "react-hot-toast";
import { useDispatch } from "react-redux";
import { setLogin } from "store/auth";
import { isErrorWithMessage, isFetchBaseQueryError } from "utils/helpers";


export default function useFbLoginController() {

    const [fbAccessToken, setFbAccessToken] = useState('');
    const [fbError, setFbError] = useState('');
    const dispatch = useDispatch();

    useEffect(() => {
        initFacebookSdk();
    }, [])

    const handleClickLogin = () => {
        try {
            window.FB.login(function (response) {
                if (response.authResponse) {
                    const { accessToken } = response.authResponse;
                    setFbAccessToken(accessToken);
                } else {
                    toast.error('User cancelled login or did not fully authorize.');
                    return;
                }
            }, { scope: 'email' });

        } catch (error) {
            console.error('Login process failed:', error);
            toast.error('Login process failed.');
            setFbError(error.message || 'An unknown error occurred');
        }
    }

    const handleFbResponse = async (accessToken, serverRequest) => {
        if (!accessToken) return;
        await serverRequest({ accessToken });
    }

    const handleServerResponse = (resData, serverError) => {
        if (serverError) {
            if (isFetchBaseQueryError(serverError)) {
                const errorMessage = serverError.data ? serverError.data.message : serverError.error
                toast.error(errorMessage)
            }
            if (isErrorWithMessage(serverError)) {
                toast.error(serverError.message)
            }
            return
        }
        if (!resData) {
            return
        }
        const userData = resData.metadata;
        dispatch(setLogin({
            userId: userData.user.userId,
            useName: userData.user.userName,
            userEmail: userData.user.email,
            accessToken: userData.tokens.accessToken,
            refreshToken: userData.tokens.refreshToken,
            authProvider: 'facebook'
        }))
    }

    return {
        fbAccessToken,
        handleClickLogin,
        handleFbResponse,
        handleServerResponse,
        fbError
    }
}
