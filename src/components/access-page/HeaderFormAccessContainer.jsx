
const HeaderFormAccessContainer = ({ classCss, children }) => {
    return (
        <div className={classCss}>
            {children}
        </div>
    );
}

export default HeaderFormAccessContainer;