import Button from "components/core/Button"
import facebookIcon from "assets/img/facebook-icon-2.svg"
import useFbLoginController from "hooks/access/useFbLoginController";
import { useLogInFbUserMutation } from "services/api/AccessService";
import { useEffect } from "react";


const FacebookLoginButton = () => {
    const { fbAccessToken, handleFbResponse, handleServerResponse, handleClickLogin } = useFbLoginController();
    const [logInFbUser, { data: serverData, error: serverError }] = useLogInFbUserMutation();
    useEffect(() => {
        handleFbResponse(fbAccessToken, logInFbUser);
    }, [fbAccessToken])

    useEffect(() => {
        handleServerResponse(serverData, serverError);
    }, [serverData, serverError])
    return (
        <Button onClick={handleClickLogin} width='100%' className='bg-transparent hover:bg-transparent text-[#385185] py-[7px] px-[16px]' >
            <span >
                <img className=" inline-block w-5 h-5 mr-2" src={facebookIcon} alt="icon-fb" />
            </span>
            Log in with Facebook
        </Button>
    );
}

export default FacebookLoginButton;