import Button from "components/core/Button";
import Modal from "components/core/Modal";
import { WALLET_FIELDS } from "configs/wallet.config";
import { useEffect, useState } from "react";
import toast from "react-hot-toast";
import { useDeleteWalletMutation, useListWalletsQuery } from "services/api/WalletService";
import { toastError } from "utils/helpers";

const DeleteWalletModal = ({ walletItem, isOnOpen, onClose, onDeleted }) => {
    const { refetch } = useListWalletsQuery();
    const [isModalOpen, setIsModalOpen] = useState(isOnOpen)
    const [currentWallet, setCurrentWallet] = useState(walletItem);
    const [deleteWallet, { data, isSuccess, error }] = useDeleteWalletMutation();

    const closeModal = (e) => {
        e.preventDefault();
        onClose();
        setIsModalOpen(false)
    }

    const handleDelete = async () => {
        await deleteWallet(currentWallet[WALLET_FIELDS.id])
    }

    useEffect(() => {
        if (error) {
            toastError(error);
        }
        if (isSuccess) {
            toast.success('wallet deleted!')
            refetch();
            onDeleted(currentWallet[WALLET_FIELDS.id])
            onClose()
            setIsModalOpen(false)
        }
    }, [error, isSuccess])

    useEffect(() => {
        setCurrentWallet(walletItem);
    }, [walletItem])

    useEffect(() => {
        setIsModalOpen(isOnOpen)
    }, [isOnOpen])

    return (
        <Modal
            isOpen={isModalOpen}
            onClose={closeModal}
            className='max-h-1/2 max-w-1/2 h-[15rem] w-[35rem] flex flex-col gap-2 items-center pt-4 px-4'
        >
            <div className="flex flex-col gap-5 justify-left min-w-64 w-full  ">
                <span className="text-left leading-7 w-full">Are you sure to delete all transactions of this wallet
                    <span type="text" className="bg-red-700 rounded-md mx-2 px-2 py-1">{currentWallet && currentWallet[WALLET_FIELDS.name]}</span>
                </span>
            </div>
            <div className=" absolute w-full bottom-5 flex flex-col justify-center items-center gap-2 px-5">
                <Button onClick={handleDelete} className='bg-red-700 px-3 py-2'>Delete All Transactions</Button>
                <Button type='button' className='bg-stone-600 px-3 py-2' onClick={closeModal}>Cancel</Button>
            </div>
        </Modal>
    );
}

export default DeleteWalletModal;