export const BUDGET_FIELDS = {
    id: '_id',
    startDate: 'start_date',
    endDate: 'end_date',
    categoryId: 'category_id',
    value: 'value',
    realValue: 'real_value',
    isRepeat: 'is_repeat'
}

export const BLANK_BUDGET = {
    [BUDGET_FIELDS.startDate]: undefined,
    [BUDGET_FIELDS.endDate]: undefined,
    [BUDGET_FIELDS.categoryId]: undefined,
    [BUDGET_FIELDS.value]: undefined,
    [BUDGET_FIELDS.isRepeat]: false
}