const appId = process.env.REACT_APP_FB_ID;
const version = process.env.REACT_APP_FB_VERSION;

export function initFacebookSdk() {
    // Ensure FB SDK is only initialized once
    if (typeof window.fbAsyncInit === 'function') return;

    // Initialize the Facebook SDK
    window.fbAsyncInit = function () {
        window.FB.init({
            appId,
            cookie: true,
            xfbml: true,
            version
        });

        // Log a page view using Facebook Analytics
        window.FB.AppEvents.logPageView();
    };

    // Load the Facebook SDK script asynchronously
    loadFacebookSdkScript();
}

function loadFacebookSdkScript() {
    let d = document, s = 'script', id = 'facebook-jssdk';
    if (d.getElementById(id)) return; // Prevent duplicate script loading

    let js = d.createElement(s),
        fjs = d.getElementsByTagName(s)[0];
    js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}




